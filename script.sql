-- Crear la base de datos
CREATE DATABASE IF NOT EXISTS prutec;

-- Entidad cliente
CREATE TABLE public.cliente (
	id bigserial NOT NULL,
	nombre varchar(255) NOT NULL,
	direccion varchar(255) NULL,
	telefono varchar(20) NULL,
	CONSTRAINT cliente_pkey PRIMARY KEY (id)
);

-- Entidad producto
CREATE TABLE public.producto (
	id bigserial NOT NULL,
	nombre varchar(255) NOT NULL,
	descripcion text NULL,
	precio float8 NOT NULL,
	stock_disponible int4 NOT NULL,
	categoria varchar(50) NULL,
	proveedor varchar(100) NULL,
	fecha_creacion date NULL,
	fecha_actualizacion date NULL,
	CONSTRAINT producto_pkey PRIMARY KEY (id)
);

-- Entidad cabecera
CREATE TABLE public.venta_cabecera (
	id bigserial NOT NULL,
	cliente_id int8 NOT NULL,
	fecha date NOT NULL,
	CONSTRAINT venta_cabecera_pkey PRIMARY KEY (id)
);
-- Cabecera foreign keys
ALTER TABLE public.venta_cabecera ADD CONSTRAINT venta_cabecera_cliente_id_fkey FOREIGN KEY (cliente_id) REFERENCES public.cliente(id);

-- Entidad Detalle
CREATE TABLE public.venta_detalle (
	id bigserial NOT NULL,
	venta_cabecera_id int8 NOT NULL,
	producto_id int8 NOT NULL,
	cantidad int4 NOT NULL,
	estado varchar(2) DEFAULT 'np'::character varying NOT NULL,
	CONSTRAINT venta_detalle_estado_check CHECK (((estado)::text = ANY ((ARRAY['np'::character varying, 'p'::character varying, 'pe'::character varying])::text[]))),
	CONSTRAINT venta_detalle_pkey PRIMARY KEY (id)
);
-- Detalle foreign keys
ALTER TABLE public.venta_detalle ADD CONSTRAINT venta_detalle_producto_id_fkey FOREIGN KEY (producto_id) REFERENCES public.producto(id);
ALTER TABLE public.venta_detalle ADD CONSTRAINT venta_detalle_venta_cabecera_id_fkey FOREIGN KEY (venta_cabecera_id) REFERENCES public.venta_cabecera(id);
