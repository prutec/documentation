# Documentación del Proyecto

Esta documentación proporciona instrucciones claras y concisas sobre cómo ejecutar los microservicios y configurar RabbitMQ para el proyecto prutec.stock.

## Configuración de RabbitMQ

Para ejecutar RabbitMQ, sigue estos pasos:

1. Descarga e instala Docker si aún no lo has hecho.
2. Ejecuta el siguiente comando para iniciar el contenedor de RabbitMQ:

```console
docker run -d --name rabbitmq-container -p 5672:5672 -p 15672:15672 rabbitmq:3-management
```

Esto creará un contenedor llamado "rabbitmq-container" con RabbitMQ ejecutándose en los puertos 5672 y 15672 para la interfaz de gestión.

3. Accede a la interfaz de gestión de RabbitMQ en tu navegador web usando la URL `http://localhost:15672`. Las credenciales predeterminadas son `guest/guest`.

## Configuración de la Base de Datos

Para ejecutar PostgreSQL en un contenedor Docker, sigue estos pasos:

1. Ejecuta el siguiente comando para iniciar el contenedor de PostgreSQL:
```console
docker run -d --name postgres-container -e POSTGRES_PASSWORD=mysecretpassword -p 5432:5432 postgres:latest
```

Esto creará un contenedor llamado "postgres-container" con PostgreSQL ejecutándose en el puerto 5432 y con la contraseña especificada.

2. Para crear la base de datos y las tablas necesarias, puedes utilizar el script SQL que se encuentra entre los archivos.

